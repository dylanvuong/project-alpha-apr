from django import forms
from projects.models import Project
from django.contrib.auth.models import User


class ProjectForm(forms.ModelForm):
    owner = forms.ModelChoiceField(queryset=User.objects.all(), required=True)

    class Meta:
        model = Project
        fields = (
            "name",
            "description",
            "owner",
        )
